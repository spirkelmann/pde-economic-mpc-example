from subprocess import call, check_output
from pathlib import Path
import numpy as np

class MPC():
    def __init__(self, N=10, exec_folder='cpp/', result_folder='results/'):
        self.N = N
        self.exec_folder = exec_folder
        self.result_folder = result_folder

        p = Path(self.result_folder)
        try:
            Path.mkdir(p)
        except Exception:
            print("file exists!")

    def solve(self, k, y0, reference=False):
        y = y0.vector().get_local()

        y_file = open(self.result_folder + "y0.txt", "w")
        y_file.write(str(len(y)) + "\n")
        for val in y:
            y_file.write(str(val) + "\n")
        y_file.close()

        call_args = [self.exec_folder + "heat", "-k{}".format(k), "--y0="+self.result_folder+"y0.txt", "-c",
                     "-N" + str(self.N), "--ov",  "--matA=A.mtx", "--matB_w=B_w.mtx", "--matB_y=B_y.mtx",
                     "--b_u=b_u.txt", "--b_y_out=b_y_out.txt", "--result_folder=" + self.result_folder,
              #"--result_folder_prefix=" + folder_prefix,
              "--y_lower=-0.15", "--y_upper=0.15",
              "--pythonparam=python_parameters.txt", "--output=0"]

        if reference:
            # free initial value in case of reference computation
            call_args.append("--fi")

        # call cpp program to solve open loop problem
        call_output = check_output(call_args)

        computation_time = read_value_from_output("total cpu time = ", call_output)

        # read results from folder
        p = Path(self.result_folder)

        # open loops
        q = p / ('openloop_y_k={}.txt'.format(k))
        if q.exists():
            y_ol = np.loadtxt(str(q), ndmin=1)
        else:
            print("open loop not found!")

        q = p / ('openloop_u_k={}.txt'.format(k))
        if q.exists():
            u_ol = np.loadtxt(str(q), ndmin=1)
        else:
            print("open loop not found!")

        q = p / ('openloop_w_k={}.txt'.format(k))
        if q.exists():
            w_ol = np.loadtxt(str(q), ndmin=1)
        else:
            print("open loop not found!")

        return y_ol, u_ol, w_ol, computation_time

def read_value_from_output(substr_indicator, output_str):
    substr_pos = output_str.find(substr_indicator)
    substr_pos_newline = output_str.find("\n", substr_pos)
    if substr_pos == -1 or substr_pos_newline == -1:
        print("error: could read value from program output!")
        return None
    else:
        value = float(output_str[substr_pos + len(substr_indicator):substr_pos_newline])
        return value
