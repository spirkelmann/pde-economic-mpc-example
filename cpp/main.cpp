/*
 * main.cpp
 *
 *  Created on: Dec 3, 2017
 *      Author: martin
 */

#include <string>
#include <vector>
#include <time.h>

#include "heat_nlp.hpp"
#include "IpIpoptApplication.hpp"
#include "IpSolveStatistics.hpp"
#include "args.hxx"


using namespace Ipopt;

int optimize(MATRIXOP &data, int outputlevel);

void write_parameters(int MPC_horizon, string matrix_A, string matrix_B_y, string matrix_B_w, string vec_b_u,
                      string vec_b_y_out, string pythonparam_, double eps, double y_ref, double u_ref, double u_upper,
                      double u_lower, double y_upper, double y_lower, double w_upper, double w_lower, bool convection,
                      bool open_values, bool free_init_value, string foldername);

int main(int argc, char **argv) {
    bool convection = false;
    bool open_values = false;
    bool free_init_value = false;

    int MPC_horizon = 100;
    int outputlevel = 0;

    double eps = 10e-3;
    double y_ref = 0.0;
    double u_ref = 0.0;
    double u_upper = 0.25;
    double u_lower = -0.25;
    double y_upper = 0.15;
    double y_lower = -0.15;
    double w_upper = 1e19;
    double w_lower = -1e19;

    int k = 0;

    string vec_y0 = "y0.txt";
    string matrix_A = "../A.mtx";
    string matrix_B_y = "../B_y.mtx";
    string matrix_B_w = "../B_w.mtx";
    string vec_b_u = "../b_u.txt";
    string vec_b_y_out = "../b_y_out.txt";
    string result_folder = "../results/";
    string result_folder_prefix = "";
    string pythonparam = "../python_parameters.txt";


    args::ArgumentParser parser("convection diffusion equation 1d.", "This goes after the options.");
    args::HelpFlag help(parser, "help", "Display this help menu", {'h', "help"});
    args::CompletionFlag completion(parser, {"complete"});

    args::Flag convection_(parser, "convection", "turn convection on", {'c', "convection"});
    args::Flag open_values_(parser, "open values", "save states of all open loop", {"ov", "openvalues"});
    args::Flag free_init_value_(parser, "free initial value", "indicates whether the intial state should be free",
                                {"fi", "free-init-value"});

    args::ValueFlag<int> k_(parser, "current step", "current time step", {'k', "time"});
    args::ValueFlag<int> MPC_horizon_(parser, "mpc horizon", "MPC horizon, N >= 1", {'N', "mpc"});
    args::ValueFlag<int> outputlevel_(parser, "outputlevel", "outputlevel of IPopt", {"output"});

    args::ValueFlag<string> vec_y0_(parser, "y0", "File containing the initial state of the PDE", {"y0"});

    args::ValueFlag<double> eps_(parser, "epsilon", "epsilon from cost functional", {'e', "eps"});
    args::ValueFlag<double> u_ref_(parser, "u_ref", "reference solution for control", {'u', "uref"});
    args::ValueFlag<double> y_ref_(parser, "y_ref", "reference solution for state", {'y', "yref"});
    args::ValueFlag<double> u_upper_(parser, "u_upper", "upper bound for control", {"u_upper"});
    args::ValueFlag<double> u_lower_(parser, "u_lower", "lower bound for control", {"u_lower"});
    args::ValueFlag<double> y_upper_(parser, "y_upper", "upper bound for state", {"y_upper"});
    args::ValueFlag<double> y_lower_(parser, "y_lower", "lower bound for state", {"y_lower"});
    args::ValueFlag<double> w_upper_(parser, "w_upper", "upper bound for convection", {"w_upper"});
    args::ValueFlag<double> w_lower_(parser, "w_lower", "lower bound for convection", {"w_lower"});

    args::ValueFlag<string> matrix_A_(parser, "Matrix A", "matrix A from PDE", {"matA"});
    args::ValueFlag<string> matrix_B_y_(parser, "Matrix B_y", "matrix B_y from PDE", {"matB_y"});
    args::ValueFlag<string> matrix_B_w_(parser, "Matrix B_w", "matrix B_w from PDE", {"matB_w"});
    args::ValueFlag<string> vec_b_u_(parser, "vec_b_u", "vector b_u from PDE", {"b_u"});
    args::ValueFlag<string> vec_b_y_out_(parser, "vec_b_y_out", "vector b_y_out from PDE", {"b_y_out"});
    args::ValueFlag<string> result_folder_(parser, "result_folder", "folder where results shall be stored",
                                           {"result_folder"});
    args::ValueFlag<string> result_folder_prefix_(parser, "result_folder_prefix", "prefix for created folder",
                                                  {"result_folder_prefix"});
    args::ValueFlag<string> pythonparam_(parser, "pythonparam", "parameter file from PDE", {"pythonparam"});


    try {
        parser.ParseCLI(argc, argv);
    }
    catch (args::Completion e) {
        std::cout << e.what();
        return 0;
    }
    catch (args::Help) {
        std::cout << parser;
        return 0;
    }
    catch (args::ParseError e) {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }

    //bool
    convection = convection_;
    open_values = open_values_;
    free_init_value = free_init_value_;

    //int
    if (MPC_horizon_) {
        MPC_horizon = args::get(MPC_horizon_);
    }
    if (outputlevel_) {
        outputlevel = args::get(outputlevel_);
    }
    if (k_) {
        k = args::get(k_);
    }

    std::cout << "time step " << k << std::endl;

    //double
    if (eps_) {
        eps = args::get(eps_);
    }
    if (u_ref_) {
        u_ref = args::get(u_ref_);
    }
    if (y_ref_) {
        y_ref = args::get(y_ref_);
    }
    if (u_upper_) {
        u_upper = args::get(u_upper_);
    }
    if (u_lower_) {
        u_lower = args::get(u_lower_);
    }
    if (y_upper_) {
        y_upper = args::get(y_upper_);
    }
    if (y_lower_) {
        y_lower = args::get(y_lower_);
    }
    if (w_upper_) {
        w_upper = args::get(w_upper_);
    }
    if (w_lower_) {
        w_lower = args::get(w_lower_);
    }

    //string
    if (vec_y0_) {
        vec_y0 = args::get(vec_y0_);
    }
    if (matrix_A_) {
        matrix_A = args::get(matrix_A_);
    }
    if (matrix_B_y_) {
        matrix_B_y = args::get(matrix_B_y_);
    }
    if (matrix_B_w_) {
        matrix_B_w = args::get(matrix_B_w_);
    }
    if (vec_b_u_) {
        vec_b_u = args::get(vec_b_u_);
    }
    if (vec_b_y_out_) {
        vec_b_y_out = args::get(vec_b_y_out_);
    }
    if (result_folder_) {
        result_folder = args::get(result_folder_);
    }
    if (result_folder_prefix_) {
        result_folder_prefix = args::get(result_folder_prefix_);
    }
    if (pythonparam_) {
        pythonparam = args::get(pythonparam_);
    }




    //start program
    MATRIXOP data(MPC_horizon, matrix_A, matrix_B_y, matrix_B_w, vec_b_u, vec_b_y_out, eps, y_ref, u_ref, u_upper,
                  u_lower, y_upper, y_lower, w_upper, w_lower, convection, open_values, free_init_value, result_folder,
                  result_folder_prefix, k, vec_y0);
    int status = optimize(data, outputlevel);

    if (status == 0) {
        cout << "problem solved \n";
    } else {
        cout << "IPopt could'nt solve the problem, use higher outputlevel to investigate \n";
        return (1);
    }


    if (open_values) {
        write_parameters(MPC_horizon, matrix_A, matrix_B_y, matrix_B_w, vec_b_u, vec_b_y_out, pythonparam, eps, y_ref,
                         u_ref, u_upper, u_lower, y_upper, y_lower, w_upper, w_lower, convection, open_values,
                         free_init_value, data.foldername);
    }

    return 0;
}

int optimize(MATRIXOP &data, int outputlevel) {
    SmartPtr<TNLP> mynlp = new HEAT_NLP(data);

    SmartPtr<IpoptApplication> app = IpoptApplicationFactory();
    app->RethrowNonIpoptException(true);

    // Change some options
    //app->Options()->SetStringValue("derivative_test", "second-order");
    //app->Options()->SetStringValue("output_file", "ipopt.out");
    app->Options()->SetIntegerValue("print_level", outputlevel);

    app->Options()->SetNumericValue("tol", 1e-5);
    app->Options()->SetStringValue("jac_c_constant", "yes");
    app->Options()->SetStringValue("jac_d_constant", "yes");
    app->Options()->SetStringValue("hessian_constant", "yes");
    app->Options()->SetStringValue("linear_solver", "ma27");

    if (data.convection) {
        app->Options()->SetStringValue("jac_c_constant", "no");
        app->Options()->SetStringValue("jac_d_constant", "no");

        //approximation of the hessian of the lagranage function by only using the hessian of the objective function
        //error in u and w of ~10e-3
        app->Options()->SetStringValue("hessian_constant", "yes");
        //app->Options()->SetStringValue("hessian_approximation", "limited-memory");
    }

    // Initialize the IpoptApplication and process the options
    ApplicationReturnStatus status;
    status = app->Initialize();
    if (status != Solve_Succeeded) {
        std::cout << std::endl << std::endl << "*** Error during initialization!" << std::endl;
        return (int) status;
    }

    // Ask Ipopt to solve the problem
    status = app->OptimizeTNLP(mynlp);

    if (status == Solve_Succeeded) {
        std::cout << " *** The problem solved!" << std::endl;

	// statistics
	
    SmartPtr<SolveStatistics> stats = app->Statistics();
	std::cout << "total cpu time = " << stats->TotalWallclockTime() << std::endl;
        //return (int) status;
    } else {
        std::cout << std::endl << std::endl << "*** The problem FAILED!" << std::endl;
        return (int) status;
    }

    //0 if successful
    return (int) status;
}


void write_parameters(int MPC_horizon, string matrix_A, string matrix_B_y, string matrix_B_w, string vec_b_u,
                      string vec_b_y_out, string pythonparam_, double eps, double y_ref, double u_ref, double u_upper,
                      double u_lower, double y_upper, double y_lower, double w_upper, double w_lower, bool convection,
                      bool open_values, bool free_init_value, string foldername) {
    double alpha = 0, beta = 0, gamma = 0;
    int n = 0;
    string trash;
    ifstream pythonparam(pythonparam_);
    if (pythonparam.is_open()) {
        pythonparam >> n >> trash >> alpha >> trash >> beta >> trash >> gamma;
        pythonparam.close();
    } else {
        cout << "can't open parameter file from python script" << endl;
        exit(1);
    }

    ofstream out(foldername + "parameters.txt");
    if (out.is_open()) {

        string function = "0.3 * sin(0.1 * i)";

        //need section for python configparser
        out << "[param]" << endl;
        out << "discretization_parameter = " << n << endl;
        out << "alpha = " << alpha << endl;
        out << "beta = " << beta << endl;
        out << "gamma = " << gamma << endl;
        out << "MPC_horizon = " << MPC_horizon << endl;

        out << "eps = " << eps << endl;
        out << "y_ref = " << y_ref << endl;
        out << "u_ref = " << u_ref << endl;
        out << "u_upper = " << u_upper << endl;
        out << "u_lower = " << u_lower << endl;
        out << "y_upper = " << y_upper << endl;
        out << "y_lower = " << y_lower << endl;
        out << "w_upper = " << w_upper << endl;
        out << "w_lower = " << w_lower << endl;

        out << "convection = " << convection << endl;
        out << "open_values = " << open_values << endl;
        out << "free_init_value = " << free_init_value << endl;

        out << "function = " << function << endl;
        out << "matrix_A = " << matrix_A << endl;
        out << "matrix_B_y = " << matrix_B_y << endl;
        out << "matrix_B_w = " << matrix_B_w << endl;
        out << "vec_b_u = " << vec_b_u << endl;
        out << "vec_b_y_out = " << vec_b_y_out << endl;

        out.close();
    } else {
        cout << "can't open paramater file to write to" << endl;
        exit(1);
    }
}
