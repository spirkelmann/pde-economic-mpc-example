from __future__ import print_function
from fenics import *
import numpy as np
import os, sys
import scipy.io
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import time
from mpc import MPC

from plots import *

from collections import OrderedDict

# Set log level
set_log_level(WARNING)

# Prepare a mesh
n = 100
mesh = UnitIntervalMesh(n)

sampling_rate = 1e-2
# Choose a time step size
dt = Constant(sampling_rate)

# MPC horizon length
N = 10

# boundary heat conductivity parameters
alpha = Constant(1.0)
beta = Constant(1.0)
gamma = Constant(1.0e3)
gamma_out = Constant(1.0e6)
delta_out = Constant(1.0e6)
gamma_c = Constant(0.0)
delta_c = Constant(1.0e1)

# Compile sub domains for boundaries
left = CompiledSubDomain("near(x[0], 0.)")
right = CompiledSubDomain("near(x[0], 1.)")

# Label boundaries, required for the objective
boundary_parts = MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
left.mark(boundary_parts, 0)    # left boundary part for outside temperature
right.mark(boundary_parts, 1)   # right boundary part where control is applied
ds = Measure("ds", subdomain_data=boundary_parts)

class VelocityFieldExpression(Expression):
    def eval(self, value, x):
        value[0] = -1.0

    def value_shape(self):
        return (1,)

def output_matrices():
    # Define function space
    parameters.linear_algebra_backend = "Eigen"

    U = FunctionSpace(mesh, "Lagrange", 1)
    W = VectorFunctionSpace(mesh, 'P', 1, dim=1)

    # Define test and trial functions
    v = TestFunction(U)
    y = TrialFunction(U)
    y0 = TrialFunction(U)

    u = Constant(1.0)
    y_out = Constant(1.0)

    w = Function(W)
    e = VelocityFieldExpression(domain=mesh, degree=1)
    w = interpolate(e, W)

    # Define variational formulation
    a = (y / dt * v + alpha * inner(grad(y), grad(v))) * dx + alpha * gamma_out * y * v * ds(0) \
        + alpha * gamma_c * y * v * ds(1)
    f_w = dot(w, grad(y)) * v * dx
    f_y = y0 / dt * v * dx

    f_y_out = alpha * delta_out * y_out * v * ds(0)

    f_u = alpha * delta_c * u * v * ds(1)

    A = assemble(a)

    B_w = assemble(f_w)
    B_y = assemble(f_y)

    b_u = assemble(f_u)
    b_y_out = assemble(f_y_out)

    # output matrices for use in matlab optimization

    A_re = as_backend_type(A).sparray()
    scipy.io.mmwrite("A.mtx", A_re, symmetry="general")

    B_w_re = as_backend_type(B_w).sparray()
    scipy.io.mmwrite("B_w.mtx", B_w_re, symmetry="general")

    B_y_re = as_backend_type(B_y).sparray()
    scipy.io.mmwrite("B_y.mtx", B_y_re, symmetry="general")

    b_u_re = b_u.array()
    b_u_file = open("b_u.txt", "w")
    b_u_file.write(str(len(b_u_re)) + "\n")
    for val in b_u_re:
        b_u_file.write(str(val) + "\n")
    b_u_file.close()

    b_y_out_re = b_y_out.array()
    b_y_out_file = open("b_y_out.txt", "w")
    b_y_out_file.write(str(len(b_y_out_re)) + "\n")
    for val in b_y_out_re:
        b_y_out_file.write(str(val) + "\n")
    b_y_out_file.close()

    param_file = open("python_parameters.txt", "w")
    param_file.write(str((n)) + " n \n")
    param_file.write(str(float((alpha))) + " alpha \n")
    param_file.write(str(float((beta))) + " beta \n")
    param_file.write(str(float((gamma))) + " gamma \n")
    param_file.close()

    return b_u, b_y_out



def solve_forward(y0, us, ws, y_outs, record=False):
    """ The forward problem """
    ofile = File("results/y.pvd")

    # Define function spaces
    U = FunctionSpace(mesh, "Lagrange", 1)
    W = VectorFunctionSpace(mesh, 'P', 1, dim=1)

    # Define test and trial functions
    v = TestFunction(U)
    y = TrialFunction(U)
    uu = Constant(1.0)
    yout = Constant(1.0)
    ww = Constant(1.0)

    e = VelocityFieldExpression(domain=mesh, degree=1)
    wfield = interpolate(e, W)

    # Define variational formulation
    a = (y / dt * v + alpha * inner(grad(y), grad(v))) * dx + alpha * gamma_out * y * v * ds(0) \
        + alpha * gamma_c * y * v * ds(1) + ww * dot(wfield, grad(y)) * v * dx
    f_y = (y0 / dt * v ) * dx
    f_y_out = alpha * delta_out * yout * v * ds(0)
    f_u = alpha * delta_c * uu * v * ds(1)

    # Prepare solution
    y = Function(U, name="y")

    y_ol = [y0.vector().get_local()]

    L = len(us)
    for i in range(0,L):
        uu.assign(us[i])
        ww.assign(ws[i])   # we need a minus here, because in the optimization the w is on the
        yout.assign(y_outs[i])

        solve(a == f_u + f_y + f_y_out, y, solver_parameters={"linear_solver":"lu"})
        y0.assign(y)

        y_ol.append(y0.vector().get_local())

    return np.array(y_ol)

def run_simulation(y0, N=1, L=50, result_folder="results/", reference=False):
    if reference:
        # compute reference solution
        N = L # horizon = full simulation window
        L = 1
        print("Starting open loop simulation")
    else:
        print("Starting closed loop simulation")

    timer_start = time.time() # start timer

    mpc = MPC(N=N, result_folder=result_folder)

    U = FunctionSpace(mesh, "Lagrange", 1)
    y = Function(U)
    yol = Function(U)
    yol_sim = Function(U)
    y.assign(y0)

    # closed loop data
    t_cl = [0.0]
    y_cl = [y.vector().get_local()]
    u_cl = []
    w_cl = []
    J_cl = []
    J_running = 0.0

    cpu_times = []

    for k in range(0, L):
        y_ol, u_ol, w_ol, cpu_time = mpc.solve(k, y, reference)

        #print("u = {}, w = {}".format(u_ol[0], w_ol[0]))

        youts = []
        for j in range(0,N):
           youts.append(0.3*sin(0.1 * (k+j)))

        y_ol_sim = solve_forward(y, u_ol, w_ol, youts)

        y.vector().set_local(y_ol_sim[1])

        t_cl.append((k+1)*sampling_rate)
        y_cl.append(y.vector().get_local())
        u_cl.append(u_ol[0])
        w_cl.append(w_ol[0])
        J_running += sampling_rate * 0.5 * (u_ol[0]**2 + w_ol[0]**2)
        J_cl.append(J_running)

        cpu_times.append(cpu_time)

    timer_end = time.time() # stop timer
    time_total = float(timer_end - timer_start)
    print("total time for simulation = {}".format(time_total))
    print("total time for optimization = {}".format(sum(cpu_times)))

    # store closed-loop data
    np.savetxt(result_folder + 'closedloop_t.txt', np.array(t_cl))
    np.savetxt(result_folder + 'closedloop_u.txt', np.array(u_cl))
    np.savetxt(result_folder + 'closedloop_w.txt', np.array(w_cl))
    np.savetxt(result_folder + 'closedloop_y.txt', np.array(y_cl))
    np.savetxt(result_folder + 'closedloop_cost.txt', np.array(J_cl))
    np.savetxt(result_folder + 'total_time.txt', np.ones(1) * time_total)

    np.savetxt(result_folder + 'ipopt_cpu_times.txt', np.array(cpu_times))

    # load simulation result from stored data
    sim_result = SimulationResult(result_folder)

    return sim_result

if __name__ == "__main__":
    # generate matrices from FEM discretization
    output_matrices()

    result_folder_global = 'results_new'

    simulate_N = True
    simulate_y = True
    simulate_ref = True
    closed_loop_videos = False
    cost_convergence = True
    cumulative_cost = True
    trajectory_convergence = True
    turnpike = True

    # simulation time
    L = 200

    # initial value of state
    U = FunctionSpace(mesh, "Lagrange", 1)
    y0 = interpolate(Expression("0.0",degree=1), U)

    #Ns = np.arange(1, 2)
    #Ns = np.array([1, 5])
    Ns = np.array([5,10,15,20,25,30,35,40,45,50,55,60,65,70,75])

    sim_results_N = []
    # run simulations for different horizon lengths N
    for N in Ns:
        print("N = {}".format(N))
        result_folder = result_folder_global + "/mpc_N={}_L={}/".format(N, L)
        if simulate_N:
            sim_result = run_simulation(y0, N, L, result_folder)
        else:
            sim_result = SimulationResult(result_folder)
            pass

        sim_results_N.append(sim_result)

    # run simulations with fixed horizon lengths but different initial value
    N = 50
    sim_results_y = []
    for i in range(0,11):
        yinit = -0.1 + 0.02*i
        print("y_init = {}".format(yinit))
        y0 = interpolate(Expression("{}".format(yinit),degree=1), U)
        result_folder = result_folder_global + "/mpc_N={}_L={}_y0={}/".format(N, 1, yinit)
        if simulate_y:
            sim_result = run_simulation(y0, N, 1, result_folder)
        else:
            sim_result = SimulationResult(result_folder)

        sim_results_y.append(sim_result)

    L_ref = L + max(Ns)
    result_folder_ref = result_folder_global + "/ref_L={}/".format(L_ref)
    if simulate_ref:
        sim_result_ref = run_simulation(y0, L=L_ref, result_folder=result_folder_ref, reference=True)
    else:
        sim_result_ref = SimulationResult(result_folder_ref)

    if closed_loop_videos:
        # closed loop simulations
        for sim_result in sim_results_N:
            sim_result.plot_closed_loop(sim_result.result_folder+"y_cl.mp4", reference=sim_result_ref)

    sim_results_filtered = filter(lambda r: r.N % 10 == 0, sim_results_N)
    if cumulative_cost:
        plot_cumulative_closed_loop_cost(sim_results_filtered, output_file='figures/cumulative_cost.eps')

    if cost_convergence:
        plot_cost_convergence(sim_results_N, output_file='figures/cost_convergence.eps')

    if trajectory_convergence:
        plot_closed_loop_convergence(sim_results_filtered, sim_result_ref, output_file='figures/trajectory_convergence.eps')
        plot_closed_loop_convergence(sim_results_filtered, sim_result_ref, output_file='figures/trajectory_convergence.eps')

    if turnpike:
        # 1. fixed initial value, two differnt horizons + closed loop
        for result in sim_results_N:
            if result.N in [25, 50]:
                plot_turnpike_cl(result, sim_result_ref, 'figures/turnpike_N={}.eps'.format(result.N), max_k=100)

        # 2. fixed initial value
        plot_turnpikes_Ns(sim_results_N, sim_result_ref, 'figures/turnpikes_varying_N.eps')

        # 3. different initial values
        plot_turnpike_ys(sim_results_y, sim_result_ref, 'figures/turnpike.eps')

